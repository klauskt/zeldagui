﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZeldaKlaus.Classes;


/*MOVE: W,D,S,A
 Attack: J*/

namespace ZeldaKlaus
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            startGame();
        }

        //Crear objetos
        Link link = new Link();
        Monster octorok = new Monster();
        Weapon Sword = new Weapon(20);
        Obstaculoss obs = new Obstaculoss();
        Objeto objTemp = new Objeto();

        //Tomar espada
        void Rules_getSword()
        {
            if ((link.tocar(sword.Left, sword.Top, 30)) && link.Espada == null)
            {
                link.Equipar(Sword);
                sword.Visible = false;
            }
        }

        //Eliminar enemigo
        void remove_enemy()
        {
            if (octorok.Vida <= 0)
            {
                enemy1.Visible = false;
                lbl_vidaE.Visible = false;
                obs.obstaculos.Remove(octorok);
            }
        }

        //Actualizar movimientos de link
        void Update_link()
        {
            linkF.Left = link.XPos;
            linkF.Top = link.YPos;
            labelX.Text = link.XPos.ToString();
            labelY.Text = link.YPos.ToString();
        }

        //Asignar valores de coordenadas
        void startGame()
        {

            link.yPos = linkF.Top;
            link.xPos = linkF.Left;

            Sword.yPos = sword.Top;
            Sword.xPos = sword.Left;

            octorok.yPos = enemy1.Top = 187;
            octorok.xPos = enemy1.Left = 77;
            obs.obstaculos.Add(octorok);

            obs.obstaculos[0].yPos = tree1.Top = 187;
            obs.obstaculos[0].xPos = tree1.Left = 197;

            obs.obstaculos[1].yPos = rock1.Top = 277;
            obs.obstaculos[1].xPos = rock1.Left = 317;
        }

        //Metodo que checa si link toca a un objeto
        void checar_todo()
        {
            foreach (Objeto obj in obs.obstaculos)
            {
                if (obj.tocar(link.xPos, link.yPos, 30))
                {
                    objTemp = obj;
                    return;
                }
            }
        }

        //Evento KeyDown
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            checar_todo(); //Busca si link toca algo, despues lo guarda en objTemp
            
            if (e.KeyCode == Keys.W)
            {
                linkF.Image = ZeldaKlaus.Properties.Resources.link_back;
                link.FaceDir = 1;

                if (link.YPos > 97)
                {
                    if (objTemp.LadoContacto != 3)
                    {
                        link.Move(1, 30);
                    }
                }

            }
            else if (e.KeyCode == Keys.S)
            {
                linkF.Image = ZeldaKlaus.Properties.Resources.link_front;
                link.FaceDir = 3;

                if (link.YPos < 367)
                {
                    if (objTemp.LadoContacto != 1)
                    {
                        link.Move(2, 30);
                    }
                }
            }
            else if (e.KeyCode == Keys.D)
            {
                linkF.Image = ZeldaKlaus.Properties.Resources.link_right;
                link.FaceDir = 2;

                if (link.XPos < 407)
                {
                    if (objTemp.LadoContacto != 4)
                    {
                        link.Move(4, 30);
                    }
                }

            }
            else if (e.KeyCode == Keys.A)
            {
                linkF.Image = ZeldaKlaus.Properties.Resources.link_left;
                link.FaceDir = 4;

                if (link.XPos > 17)
                {
                    if (objTemp.LadoContacto != 2)
                    {
                        link.Move(3, 30);
                    }
                }

            }
            else if (e.KeyCode == Keys.J)
            {
                if (!(link.Espada == null))
                {
                    if ((link.LadoContacto == link.FaceDir))
                    {
                        link.Atacar(octorok);
                        lbl_vidaE.Text = "Vida enemigo "+ octorok.Vida.ToString();
                    }

                }
            }

            Update_link(); //Mueve a link
            link.tocar(octorok.xPos, octorok.yPos, 30); // Registra por donde fue tocado link
            lblDir.Text = link.FaceDir.ToString(); //Direccion a donde voltea link
            lblCon.Text = link.LadoContacto.ToString(); //Lado del contacto con link
            objTemp.LadoContacto = 0;

            Rules_getSword();//Tomar espada. Si no tengo espada

            remove_enemy();//Eliminar enemigo si murió
        }
    }
}
