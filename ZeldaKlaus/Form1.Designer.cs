﻿namespace ZeldaKlaus
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sword = new System.Windows.Forms.PictureBox();
            this.enemy1 = new System.Windows.Forms.PictureBox();
            this.linkF = new System.Windows.Forms.PictureBox();
            this.menuZelda = new System.Windows.Forms.PictureBox();
            this.labelX = new System.Windows.Forms.Label();
            this.labelY = new System.Windows.Forms.Label();
            this.lblDir = new System.Windows.Forms.Label();
            this.lblCon = new System.Windows.Forms.Label();
            this.lbl_vidaE = new System.Windows.Forms.Label();
            this.tree1 = new System.Windows.Forms.PictureBox();
            this.rock1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.sword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuZelda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tree1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rock1)).BeginInit();
            this.SuspendLayout();
            // 
            // sword
            // 
            this.sword.Image = global::ZeldaKlaus.Properties.Resources.sword;
            this.sword.Location = new System.Drawing.Point(214, 475);
            this.sword.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.sword.Name = "sword";
            this.sword.Size = new System.Drawing.Size(40, 48);
            this.sword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.sword.TabIndex = 3;
            this.sword.TabStop = false;
            // 
            // enemy1
            // 
            this.enemy1.Image = global::ZeldaKlaus.Properties.Resources.octorok;
            this.enemy1.Location = new System.Drawing.Point(146, 585);
            this.enemy1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.enemy1.Name = "enemy1";
            this.enemy1.Size = new System.Drawing.Size(60, 58);
            this.enemy1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.enemy1.TabIndex = 2;
            this.enemy1.TabStop = false;
            // 
            // linkF
            // 
            this.linkF.Image = global::ZeldaKlaus.Properties.Resources.link_right;
            this.linkF.Location = new System.Drawing.Point(394, 475);
            this.linkF.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.linkF.Name = "linkF";
            this.linkF.Size = new System.Drawing.Size(50, 58);
            this.linkF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.linkF.TabIndex = 1;
            this.linkF.TabStop = false;
            // 
            // menuZelda
            // 
            this.menuZelda.Image = global::ZeldaKlaus.Properties.Resources.menu_zelda;
            this.menuZelda.Location = new System.Drawing.Point(-8, -4);
            this.menuZelda.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.menuZelda.Name = "menuZelda";
            this.menuZelda.Size = new System.Drawing.Size(882, 171);
            this.menuZelda.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.menuZelda.TabIndex = 0;
            this.menuZelda.TabStop = false;
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Location = new System.Drawing.Point(56, 181);
            this.labelX.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(72, 25);
            this.labelX.TabIndex = 4;
            this.labelX.Text = "labelX";
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(140, 181);
            this.labelY.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(73, 25);
            this.labelY.TabIndex = 5;
            this.labelY.Text = "labelY";
            // 
            // lblDir
            // 
            this.lblDir.AutoSize = true;
            this.lblDir.Location = new System.Drawing.Point(656, 173);
            this.lblDir.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblDir.Name = "lblDir";
            this.lblDir.Size = new System.Drawing.Size(87, 25);
            this.lblDir.TabIndex = 6;
            this.lblDir.Text = "FaceDir";
            // 
            // lblCon
            // 
            this.lblCon.AutoSize = true;
            this.lblCon.Location = new System.Drawing.Point(774, 173);
            this.lblCon.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblCon.Name = "lblCon";
            this.lblCon.Size = new System.Drawing.Size(98, 25);
            this.lblCon.TabIndex = 7;
            this.lblCon.Text = "Contacto";
            // 
            // lbl_vidaE
            // 
            this.lbl_vidaE.AutoSize = true;
            this.lbl_vidaE.Location = new System.Drawing.Point(394, 181);
            this.lbl_vidaE.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lbl_vidaE.Name = "lbl_vidaE";
            this.lbl_vidaE.Size = new System.Drawing.Size(149, 25);
            this.lbl_vidaE.TabIndex = 8;
            this.lbl_vidaE.Text = "Vida enemigo:";
            // 
            // tree1
            // 
            this.tree1.BackColor = System.Drawing.SystemColors.Menu;
            this.tree1.Image = global::ZeldaKlaus.Properties.Resources.tree_sprite;
            this.tree1.Location = new System.Drawing.Point(574, 544);
            this.tree1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.tree1.Name = "tree1";
            this.tree1.Size = new System.Drawing.Size(50, 48);
            this.tree1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.tree1.TabIndex = 9;
            this.tree1.TabStop = false;
            // 
            // rock1
            // 
            this.rock1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.rock1.Location = new System.Drawing.Point(661, 410);
            this.rock1.Name = "rock1";
            this.rock1.Size = new System.Drawing.Size(50, 48);
            this.rock1.TabIndex = 10;
            this.rock1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 790);
            this.Controls.Add(this.rock1);
            this.Controls.Add(this.tree1);
            this.Controls.Add(this.lbl_vidaE);
            this.Controls.Add(this.lblCon);
            this.Controls.Add(this.lblDir);
            this.Controls.Add(this.labelY);
            this.Controls.Add(this.labelX);
            this.Controls.Add(this.sword);
            this.Controls.Add(this.enemy1);
            this.Controls.Add(this.linkF);
            this.Controls.Add(this.menuZelda);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.sword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.menuZelda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tree1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rock1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox menuZelda;
        private System.Windows.Forms.PictureBox linkF;
        private System.Windows.Forms.PictureBox enemy1;
        private System.Windows.Forms.PictureBox sword;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label lblDir;
        private System.Windows.Forms.Label lblCon;
        private System.Windows.Forms.Label lbl_vidaE;
        private System.Windows.Forms.PictureBox tree1;
        private System.Windows.Forms.PictureBox rock1;
    }
}

