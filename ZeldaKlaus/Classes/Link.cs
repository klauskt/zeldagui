﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZeldaKlaus.Classes
{
    class Link : Objeto
    {
        //Fields
        Weapon espada;
        int faceDir;

        public int XPos { get => xPos; set => xPos = value; }
        public int YPos { get => yPos; set => yPos = value; }
        internal Weapon Espada { get => espada; set => espada = value; }
        public int FaceDir { get => faceDir; set => faceDir = value; }

        //Constructor
        public Link()
        {
            Espada = null;
            faceDir = 3;
        }

        //Equiar arma
        public void Equipar(Weapon arma)
        {
            Espada = arma;
        }

        //Ataca monster, checando que lo este tocando
        public void Atacar(Monster monster)
        {
            if (tocar(monster.xPos, monster.yPos, 30))
            {
                monster.bajarVida(Espada.Damage);
            }

        }

        //Mueve a link, modificando sus coordenadas
        public void Move(int direccion, int metros)
        {

            int newY = yPos;
            int newX = xPos;

            switch (direccion)
            {
                case 1:
                    newY -= metros;
                    break;

                case 2:
                    newY += metros;
                    break;

                case 3:
                    newX -= metros;
                    break;

                case 4:
                    newX += metros;
                    break;
            }

            yPos = newY;
            xPos = newX;
        }
    }
}
