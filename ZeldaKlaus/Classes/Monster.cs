﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZeldaKlaus.Classes
{
    class Monster : Objeto
    {
        int vida;

        public Monster()
        {
            vida = 100;
        }

        public int Vida { get => vida; }

        public void bajarVida(int valor)
        {
            if(vida > 0)
            {
                vida -= valor;
            }
            
        }
    }
}
