﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZeldaKlaus.Classes
{
    class Obstaculoss
    {
        public List<Objeto> obstaculos;

        public Obstaculoss()
        {
            obstaculos = new List<Objeto>();
            obstaculos.Add(new Obstaculo("Tree"));
            obstaculos.Add(new Obstaculo("Rock"));
        }
    }
}
