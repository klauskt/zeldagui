﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZeldaKlaus.Classes
{
    class Weapon : Objeto
    {
        int damage;

        public Weapon(int damage)
        {
            this.damage = damage;
        }

        public int Damage { get => damage; }
    }
}
