﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZeldaKlaus.Classes
{
    class Objeto
    {
        //Todos los objetos tienen coordenadas.
        //Todas las clases heredan de esta
        public int xPos;
        public int yPos;
        int ladoContacto;

        public int LadoContacto { get => ladoContacto; set => ladoContacto = value; }

        //Todos los objetos tienen la opcion de ver si tienen contacto con algo
        public bool tocar(int x, int y, int metros)
        {
            if (xPos + metros == x && yPos == y)
            {
                LadoContacto = 2;
                return true;
            }
            else if (xPos - metros == x && yPos == y)
            {
                LadoContacto = 4;
                return true;
            }
            else if (yPos + metros == y && xPos == x)
            {
                LadoContacto = 3;
                return true;
            }
            else if (yPos - metros == y && xPos == x)
            {
                LadoContacto = 1;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
